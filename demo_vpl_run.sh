#! /bin/bash
cat > vpl_execution << 'EOF'
#----------------------------------------------------------------
#! /bin/bash
#vFile, app_vApp
VPL_REPO="http://www.dei.isep.ipp.pt/~joc/"
VPL_TMP_FLDR="tmp_$RANDOM"

clean_n_exit (){
	#clean:	echo "clean, delete $VPL_TMP_FLDR/* and rmdir $VPL_TMP_FLDR"
	rm $VPL_TMP_FLDR/*
	rmdir $VPL_TMP_FLDR
	exit
}

get_vpl_version (){
    echo $(grep "#app" vpl.version -s | cut -f2 -d"=")
}

if [ ! -f "vpl.version" ]; then
    #echo "GET DEI"
	VPL_VER_FIL="vpl.version"
    VPL_REPO="http://www.dei.isep.ipp.pt/~joc/"
    curl -s -k -L -o vpl.version "$VPL_REPO$VPL_VER_FIL" > /dev/null
	v_fil_vpl_ver=$(( $(grep "404 Not Found" vpl.version -s | wc -l) ))
	VPL_VER=$(get_vpl_version)
    if [ ! -f "vpl.version" ] || [ -z "$VPL_VER" ]; then
        #echo "GET FTP"
		#Include file@ 000webhost
        VPL_REPO="https://aprog-vpl-isep.000webhostapp.com/"
		curl -s -k -L -o vpl.version "$VPL_REPO$VPL_VER_FIL" > /dev/null
	fi
fi

VPL_VER=$(get_vpl_version)

if [ -z "$VPL_VER" ]; then
    echo "Critical error, version file not found or malformed. Exiting."
    exit
fi

#make tmp folder and get include files
mkdir -p $VPL_TMP_FLDR

#fetch files from server
curl -s -k -L -o $VPL_TMP_FLDR/vpl_prm.sh "$VPL_REPO$VPL_VER/vpl_prm.sh"
curl -s -k -L -o $VPL_TMP_FLDR/vpl_run_include.sh "$VPL_REPO$VPL_VER/vpl_run_include.sh"
curl -s -k -L -o $VPL_TMP_FLDR/vpl_php.php "$VPL_REPO$VPL_VER/vpl_php.php"

#validar ficheiros válidos
v_fil_siz_prm=$(( $(cat $VPL_TMP_FLDR/vpl_prm.sh | wc -l) ))
v_fil_siz_include=$(( $(cat $VPL_TMP_FLDR/vpl_run_include.sh | wc -l) ))
v_fil_siz_php=$(( $(cat $VPL_TMP_FLDR/vpl_php.php | wc -l) ))
if [ $v_fil_siz_prm -eq 0 ] || [ $v_fil_siz_include -eq 0 ] || [ $v_fil_siz_php -eq 0 ]; then
	echo "Erro ao carregar os ficheiros de validações."
	clean_n_exit
else
	#make exec
	chmod +x $VPL_TMP_FLDR/*
	#carregar ficheiros prm, include e php
    . ./$VPL_TMP_FLDR/vpl_prm.sh
fi

###############################################################
### PARAMETERS INI											###
###############################################################
declare -a validations_menu=("cls" "gui" "fnc" "lin" "met" "cns" "met_rtrn1" "met_nam1")
declare -a validations_todo=( "1"   "1"   "0"   "0"   "0"   "0"      "0"         "0")
declare -a validations_error=("1"   "1"   "0"   "0"   "0"   "0"      "0"         "0")

#DEBUG php 0:false, 1:true
#v_D_php=1

v_cns_min=1

v_rtrn_nam1_name="lerFuncionarios"
v_rtrn_nam1_min=1
v_rtrn_nam1_err_msg="O número de Returns do método $v_rtrn_nam1_name 
está fora do intervalo mínimo e máximo estipulados.
Obrigado.
"


###############################################################
### PARAMETERS EXAMPLE - COMMENTED	: ' ... ' - /vpl_prm.sh ###
###############################################################
: '

#DEBUG 
#php 0:false, 1:true
v_D_php=0

#if cls=1
#valida se é possivel compilar o Java 
v_cls_err_msg="A Classe e o nome do ficheiro são diferentes.
Corrija a sua aplicação, e volte a submeter.
Obrigado.
"

#if gui=1
#controla imports de GUI
v_gui_err_msg="O uso das bibliotecas 'javax.swing.*' e 'javafx.*' não é possível no âmbito deste laboratório.
Reescreva a sua aplicação, sem o recurso a estas bibliotecas, e volte a submeter.
Obrigado.
"

#if fnc=1
#se o metodo listado está presente no java, e se aparece, pelo menos, o numero de vezes definido
v_fnc_name="nomeDaFuncao"
v_fnc_max=2
v_fnc_err_msg="O método $v_fnc_name não foi encontrado.
Reescreva a sua aplicação.
Obrigado.
"

#if lin=1
#número de linhas do programa atual é de, no máximo o valor listado
v_lin_max=40
v_lin_err_msg="O número de linhas $v_num_lin supera o número de linhas esperado.
Reescreva a sua aplicação de modo a ter cerca de $v_num_lin_max linhas.
Obrigado.
"

#if met=1
#numero de metodos criados estão entre o mínimo e máximo estipulado
v_met_min=2
v_met_max=100
v_met_err_msg="O número de métodos está fora do intervalo de
métodos mínimos e máximos estipulados
Obrigado.
"

#if cns=1
#numero de constantes criadas estão entre o mínimo e máximo estipulado
v_cns_min=0
v_cns_max=100
v_cns_err_msg="O número de constantes está fora do 
intervalo mínimo e máximo estipulados.
Obrigado.
"

#if met_rtrn1=1
#numero de "return"'s no método especificado está entre os valores definidos abaixo
v_rtrn_nam1_name="nomeDoMetodo"
v_rtrn_nam1_min=0
v_rtrn_nam1_max=100
v_rtrn_nam1_err_msg="O número de Returns do método $v_rtrn_nam1_name 
está fora do intervalo mínimo e máximo estipulados.
Obrigado.
"

#if met_nam1=1
#número de linhas do método, com o nome especificado está entre o tamanho definido
v_met_nam1_name="nomeDoMetodo"
v_met_nam1_min=20
v_met_nam1_max=100
v_met_nam1_err_msg="O número de linhas do método $v_met_nam1_name 
está fora do intervalo mínimo e máximo estipulados.
Obrigado.
"

'
###############################################################
### PARAMETERS END											###
###############################################################

#run validation with updated params
. ./$VPL_TMP_FLDR/vpl_run_include.sh

#FIM
clean_n_exit
#----------------------------------------------------------------
EOF
chmod +x vpl_execution