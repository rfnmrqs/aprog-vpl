#!/bin/bash
#prm_1_0_0
#################################################################
### INITS
#################################################################

dt=$(date '+%d/%m/%Y %H:%M:%S');
declare -a validations_menu=("cls" "gui" "fnc" "lin" "met" "cns" "met_nam1")
declare -a validations_todo=("1" "1" "0" "0" "0" "1" "0")
#warning=0, critical=1
declare -a validations_error=("1" "1" "1" "0" "1" "1" "0")

#DEBUG 
#php 0:false, 1:true
v_D_php=0

#if cls=1
#valida se é possivel compilar o Java 
v_cls_err_msg="A Classe e o nome do ficheiro são diferentes.
Corrija a sua aplicação, e volte a submeter.
Obrigado.
"

#if gui=1
#controla imports de GUI
v_gui_err_msg="O uso das bibliotecas 'javax.swing.*' e 'javafx.*' não é possível no âmbito deste laboratório.
Reescreva a sua aplicação, sem o recurso a estas bibliotecas, e volte a submeter.
Obrigado.
"

#if fnc=1
#se o metodo listado está presente no java, e se aparece, pelo menos, o numero de vezes definido
v_fnc_name="lerNumeroPositivo"
v_fnc_max=2
v_fnc_err_msg="O método $v_fnc_name não foi encontrado.
Reescreva a sua aplicação.
Obrigado.
"

#if lin=1
v_lin_max=40
v_lin_err_msg="O número de linhas $v_num_lin supera o número de linhas esperado.
Reescreva a sua aplicação de modo a ter cerca de $v_num_lin_max linhas.
Obrigado.
"

#if met=1
#numero de metodos criados estão entre o mínimo e máximo estipulado
v_met_min=2
v_met_max=100
v_met_err_msg="O número de métodos está fora do intervalo de
métodos mínimos e máximos estipulados
Obrigado.
"

#if cns=1
#numero de constantes criadas estão entre o mínimo e máximo estipulado
v_cns_min=0
v_cns_max=100
v_cns_err_msg="O número de constantes está fora do 
intervalo mínimo e máximo estipulados.
Obrigado.
"

#if met_nam1=1
v_met_nam1_name=""
v_met_nam1_min=20
v_met_nam1_max=100
v_met_nam1_err_msg="O número de linhas do método $v_met_nam1_name 
está fora do intervalo mínimo e máximo estipulados.
Obrigado.
"