#v6.6.1->app_v1.0.1
#echo "<<< Início vpl_run.sh @ $dt >>>"
v_gui_dflt_err="Erro na validação"

#################################################################
### PHP
#################################################################
build_php(){
./$VPL_TMP_FLDR/vpl_php.sh $v_i.java
}


#################################################################
### VALIDATION
#################################################################
validate (){
local v_cnt=0

#echo "$validation"
case $validation in 
cls)
	v_cnt=$(( $(grep -i "class $v_i[ ]*[{]" $v_i.java | grep -v '^//' | wc -l) ))

	if [ $v_cnt -lt 1 ]; then
		echo "$v_cls_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;

gui)
	v_cnt=$(( $(grep -i "import javax.swing.*" $v_i.java | grep -v '^//' | wc -l)+$(grep -i "import javafx.*" $v_i.java | grep -v '^//' | wc -l) ))
	
	if [ $v_cnt -gt 0 ]; then
		echo "$v_gui_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;

fnc)
	v_cnt=$(( $(grep -io "$v_fnc_name[ ]*[(]" $v_i.java | wc -l) ))

	if [ $v_cnt -lt $(($v_fnc_max)) ]; then
		echo "$v_fnc_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;

lin)
#TODO , limpar linhas vazias
	v_cnt=$(( $(grep -vi "[ ]*/\*\|[ ]*//\|[ ]*\*\|[ ]*\*/" $v_i.java | wc -l) ))
	v_num_lin=$v_cnt

	if [ $v_cnt -gt $(( $v_lin_max )) ]; then
		echo "$v_lin_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;

met)
	#ORI:v_cnt=$(( $(grep -i "private\|friendly\|package\|protected\|public" $v_i.java | grep -vi "main" | grep -v ";" | grep "[()]"  | grep -i "[{]" | wc -l) ))
	#v2:v_cnt=$(( $(awk "/private/||/public/||/friendly/||/package/||/protected/,/{/" $v_i.java | tr -d '\n' | grep -vi "main" | grep -v ";" | grep "[()]"  | grep -i "[{]" | wc -l) ))
	#v3:php:

	build_php
	echo "$php" > tmp.php
	php tmp.php
	if [ $v_D_php = 1 ]; then
		cat php_res.tmp
	fi
	v_cnt=$(cat php_res.tmp | wc -l)

	if [ $v_cnt -lt $(( $v_met_min )) ] || [ $v_cnt -gt $(( $v_met_max )) ]; then
		echo "$v_met_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;

cns)
	#BKP:v_cnt=$(( $(grep -i " final[[:print:]]" $v_i.java | grep -vi "main" | grep ";" | grep -v "[{()}]" | wc -l) ))
	v_cnt=$(( $(egrep ".*(final|FINAL) (([a-z]|[A-Z])+[ ])*[A-Z0-9_]+[ ]*[=].*" $v_i.java | grep -vi "main" | grep ";" | grep -v "[{()}]" | wc -l) ))

	if [ $v_cnt -lt $(( $v_cns_min )) ] || [ $v_cnt -gt $(( $v_cns_max )) ]; then
		echo "$v_cns_err_msg"
		if [ $v_err = "1" ]; then
			#echo ">>> Fim vpl_run.sh @ $dt <<<"
			exit
		fi
	fi
;;


met_rtrn1)
	build_php
	echo "$php" > tmp.php
	php tmp.php
	if [ $v_D_php = 1 ]; then
		cat php_res.tmp
	fi
	php tmp.php

	for res in $(cat php_res.tmp)
	do 
	
		v_met=$(echo "$res" | cut -f 1 -d":")
		v_cnt=$(echo "$res" | cut -f 4 -d":")

		if [ $v_met = $v_rtrn_nam1_name ]; then
			#se for o metodo listado, averiguar lin min e max, break no fim
			if [ $v_cnt -lt $(( $v_rtrn_nam1_min )) ] || [ $v_cnt -gt $(( $v_rtrn_nam1_max )) ]; then
				echo "$v_rtrn_nam1_err_msg"
				if [ $v_err = "1" ]; then
					#echo ">>> Fim vpl_run.sh @ $dt <<<"
					exit
				fi
			fi
			break
		fi
	done
	
;;


met_nam1)
	build_php
	echo "$php" > tmp.php
	php tmp.php
	if [ $v_D_php = 1 ]; then
		cat php_res.tmp
	fi
	php tmp.php

	for res in $(cat php_res.tmp)
	do 
	
		v_met=$(echo "$res" | cut -f 1 -d":")
		v_cnt=$(echo "$res" | cut -f 2 -d":")

		if [ $v_met = $v_met_nam1_name ]; then
			#se for o metodo listado, averiguar lin min e max, break no fim
			if [ $v_cnt -lt $(( $v_met_nam1_min )) ] || [ $v_cnt -gt $(( $v_met_nam1_max )) ]; then
				echo "$v_met_nam1_err_msg"
				if [ $v_err = "1" ]; then
					#echo ">>> Fim vpl_run.sh @ $dt <<<"
					exit
				fi
			fi
			break
		fi
	done
	
;;


*)
	echo "Validação requirida '$validacao' não encontrada"
;;
esac

}


#################################################################
### SCRIPT
#################################################################
v_file=$(find . -iname "*.java" -type f  | cut -f2 -d "." | tr -d '/')
v_class=""
v_num_lin_max=40

for v_i in $v_file
do
	cp $v_i.java $v_i.bkp
	#do clean "package"
	sed -e 's/.*package.*;.*//' $v_i.java > tmp.fil
	mv -f tmp.fil $v_i.java
	#do clean "//comented lines"
	sed -e 's/ *\/\/.*.*//' $v_i.java > tmp.fil
	mv -f tmp.fil $v_i.java
	#do clean multi-line uniline comment
	sed -e 's/ *\/\*.*.*\*\///' $v_i.java > tmp.fil
	mv -f tmp.fil $v_i.java
	#do clean multi-line comment
	sed 's|/\*|\n&|g;s|*/|&\n|g' $v_i.java | sed '/\/\*/,/*\//d' > tmp.fil
	mv -f tmp.fil $v_i.java

	#fetch ".. main(", and illegal imports
	v_cnt=0

	v_num_lin=0
	
	v_cnt=$(grep -i "[[:alpha:]] main.*[(]" $v_i.java | wc -l)
	
	
	
	if [ $v_cnt -gt 0 ]; then
		#if found we have a class
		v_class=$v_i

		#do validate
		for (( v_ii=0; v_ii<${#validations_menu[@]}; v_ii++ ));
		do
			if [ ${validations_todo[$v_ii]} = "1" ]; then
				validation="${validations_menu[$v_ii]}"
				v_err="${validations_error[$v_ii]}"
				eval php=\"\$php_ori\"
				validate
			fi
		done
	fi

	#prepare for runtime
	sed -e 's/.*package.*;.*//' $v_i.bkp > tmp.fil
	mv -f tmp.fil $v_i.java

done
rm tmp.fil 2> /dev/null

#################################################################
### RUN 
#################################################################
case $v_D in
1)
	echo "ok"
;;
*)
	if [ -z "$v_class" ]; then
	    echo "Classe não encontrada."
	else
		if [ ! -f $v_class.class ] || [[ $v_class.java -nt $v_class.class ]]; then
			javac -J-Xmx128m $v_class.java
		fi
		java $v_class
	fi
;;
esac

