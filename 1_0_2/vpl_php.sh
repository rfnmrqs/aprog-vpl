#!/usr/bin/php
<?php
//php_v2.1.2->app_v1.0.2
error_reporting(E_ERROR);
$file=$argv[1];
$needle=array("public","private","friendly","package","protected");
$otext=$text=file_get_contents($file);
//init minstartpos
$minstartpos=strlen($text);
$print_at_least_one_result=FALSE;
if($argv[2]=="D")$D=TRUE; else $D=FALSE;
//reset temp file
file_put_contents("php_res.tmp","");
if(!$argv[1])exit;
print $argv[2];
if($D){
	print "    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++     \n";
	print "@@@ Start Cycle\n";
}
for(;strlen($text)>0 && $i++<100;){
	if($D){
		print "  @@@ Cycle, i:$i, StrLen(text):".strlen($text)."\n";
		print "±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±\n";
		print "±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±\n\n";
		print $text;
		print "\n±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±\n";
		print "±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±\n";

	}
	foreach ($needle as $pin) {
		//Fetch first value from $needle found in string
		$startpos=strpos($text, $pin);
		if($D){
			print "  ~~~ Vars-A, Pin:$pin, Condition:MinStartpos>StartPos:".$minstartpos.">".$startpos." AND StartPos!=FALSE:".var_export($startpos,true)."\n";
		}
		if($startpos!=FALSE && $minstartpos>$startpos)$minstartpos=$startpos;
	}
	
	if($D){
		print "  @@@ Cycle, Start Function Call! MinStartpos:$minstartpos\n";
	}
	$ostr=strlen($text);
	list($endpos,$diff,$ismethod,$methodname,$methodfullname,$methodnumlines,$methodnumreturns)=isThisAMethod($text,$minstartpos);
	
	if($D){
		print "  @@@ Cycle, End Function Call! EndPos:$endpos, Diff:$diff, IsMethod:$ismethod, MethodName:$methodname, MethodFullName:$methodfullname, MethodNumLines:$methodnumlines\n";
		print "  ~~~ Vars-B, MinStartpos:".strlen(substr($text, $endpos))."\n";
	}
	//
	if($diff<0){
	//if($startpos==strpos($text, $pin)){
	//if($endpos==$ostr){
		if($D){
			print "XXX Break!, Diff<0:".$diff.", StartPos==ActualPin($pin)Pos:".$startpos."==".strpos($text, $pin)."\n";
		}
		break;
	}
	$text=substr($text, $endpos);
	//REinit minstartpos
	$minstartpos=strlen($text);
	$nstr=strlen($text);

	if($ismethod){
		$print_at_least_one_result=TRUE;
		if($D)print $methodname.":".$methodnumlines."\n";
		else file_put_contents("php_res.tmp",file_get_contents("php_res.tmp").$methodname.":".$methodnumlines.":return:".$methodnumreturns."\n");
	}
}

if($D){
	print "@@@ End Cycle, Break OR (strlen(text)<=0:".strlen($text)." || i>=10:".$i."), PrintAtLestOneResult:$print_at_least_one_result\n";
	print "    ---------------------------------------------------------     \n";
}




function isThisAMethod($text,$startpos){
	global $D;

	$stage=0;
	$endpos=$startmethod=0;
	$is_a_method=FALSE;
	$method_cntnt="";
	if($D){print "     >>> String:\n     >>>  -----\n     >>> ";}
	for($i=(int)$startpos;$i<strlen($text) && $stage!==FALSE;$i++){
		$c=$text{$i};
		if($D){
			print $c;
			if($stage>=0 && $stage<=3)$last_good_stage=$stage;
		}
		switch ($stage) {
			case 0:
				//char must be text, space or "("
				//char must not be ")" nor "{" nor "}"
				//OK:$stage++;, NOK:$stage=-1;
				if(in_array($c, array(")","{","}"))){
					if($D)print "\n     >>>  KKK - KILL CHAR - char:'".$c."'@i:$i, Stage:$stage\n";
					$stage=-1;
				}
				else if(in_array($c, array("("))){
					$stage++;						
				}
				else
					$methodname.=$c;
				break;
			case 1:
				//char must be text, space or ")"
				//OK:$stage++;, NOK:$stage=-1;
				if(in_array($c, array("{","}",";"))){
					if($D)print "\n     >>>  KKK - KILL CHAR - char:'".$c."'@i:$i, Stage:$stage\n";
					$stage=-1;
				}
				else if(in_array($c, array(")")))
					$stage++;
				break;
			case 2:
				//char must be space or "{"
				//OK:$stage++;, NOK:$stage=-1;
				if(in_array($c, array("}","(",")",";"))){
					if($D)print "\n     >>>  KKK - KILL CHAR - char:'".$c."'@i:$i, Stage:$stage\n";
					$stage=-1;
				}
				else if(in_array($c, array("{"))){
					$stage++;
					$startmethod=$i;
					$cntr=array("p"=>0,"c"=>1,"l"=>0);
				}
				break;
			case 3:
				//char must be any char, but "(" and ")" and "{" and "}" should be pair
				//when at least 1 for pair, end of method
				//if EOF is reached prior to pair up, not a method
				//OK:$stage=FALSE;, $endpos=$curpos, NOK:$stage=-1;
				$method_cntnt.=$c;
				if(in_array($c, array("}"))){
					$cntr["c"]--;
					if($cntr["c"]==0){
						//count returns
						$method_word_return_count=0;
						$tmp=$method_cntnt;
						while($iii=stripos($tmp,"return") !== false) {
							$method_word_return_count++;
							$tmp=substr($tmp,0,$iii+strlen("return"));
						}
						//endmethod
						$endpos=$endmethod=$i;
						if($D)print "\n     >>>  KKK - OK END - char:'".$c."'@$i, ep:$endpos from ".strlen($text).", sp:$startpos, Stage:$stage, MethodFulName:".trim($methodname)."\n";
						$is_a_method=TRUE;
						$stage=FALSE;
					}
				}else if(in_array($c, array("{"))){
					$cntr["c"]++;
				}
				else if(in_array($c, array("("))){
					$cntr["p"]++;
				}
				else if(in_array($c, array(")"))){
					$cntr["p"]--;
				}
				else if($c == PHP_EOL){
					$cntr["l"]++;				
				}
				break;
			default:
				if($D)print "\n     >>>  KKK - KILL END - char:'".trim($c)."'@$i, ep:$endpos, sp:$startpos\n";
				//is not a method
				//$endpos=$startpos;
				$endpos=$i;
				$is_a_method=$stage=FALSE;
				break;
		}
	}
	if($D){
		print "     >>> \n     >>>  -----\n";
		print "     >>> Exit @ Last Good Stage:".$last_good_stage.", StartPos:$startpos , EndPos:$endpos, EndFile:".strlen($text)."\n";
	}

	$is_a_method=(int)$is_a_method;
	$methodfullname=trim($methodname);
	$methodname=array_pop(explode(" ",$methodfullname));
	if(!$is_a_method && $endpos==0)$endpos+=strlen($methodname);

	//print_r(array($methodname,(int)$is_a_method,$endpos-$startpos,$endpos,$cntr["l"]));
	//return array($is_a_method,$endpos-$startmethod,$endpos);
	if($D){
		print "^^^ RETURN - EndPos:$endpos, Diff:".($endpos-$startpos).", IsAMethod:".$is_a_method.", MethodName:".$methodname.", MethodFullName:".$methodfullname.", MethodNumLines".($cntr["l"]-1);
	}
	return array($endpos,$endpos-$startpos, $is_a_method, $methodname, $methodfullname, --$cntr["l"], $method_word_return_count);
}


?>